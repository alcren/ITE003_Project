/*
 * NAME: MERCADO, EFREN JR P.
 * PROGRAM/SECTION: CS/CS12FB1
 * FINAL PROJECT
 * DATE: 5/19/16
 */
import java.util.Scanner;
public class Main
{
    public static final Scanner input = new Scanner(System.in);
     public static char repeatOption = 'Y';
    public static void main (String[] args) throws InterruptedException {
        displayLogin();
        do{
            displayMain();
            System.out.print("\n\nDo you want to repeat the program? [Y/N]");
            repeatOption = input.next().charAt(0);
        }while(repeatOption == 'y' || repeatOption == 'Y');
        System.out.print('\u000c');
        System.out.print("Thank you!");
        Thread.sleep(3000);
        System.out.print('\u000c');
    }
    // ***********   MENU METHODS   ************
    public static void displayLogin() throws InterruptedException{
        boolean isCorrect = false;
        do{
            System.out.println("================================================");
            System.out.println("\t<< LOG IN FORM >>");
            System.out.print("Enter USERCODE: ");
            int userCode = input.nextInt();
            System.out.print("Enter PASSCODE: ");
            int passCode = input.nextInt();
            if(userCode == 111 && passCode == 222){
                isCorrect = true;
                System.out.print('\u000c');
                System.out.println("================================================");
                Thread.sleep(50);
                System.out.println("[ Programmed by: Efren Mercado Jr\t\t]");
                Thread.sleep(50);
                System.out.println("[ ITE003 Final Project\t\t\t\t]");
                Thread.sleep(50);
                System.out.println("[ Summer 2016\t\t\t\t\t]");
                Thread.sleep(50);
                System.out.println("[ Product Description: Compilation of searching ]");
                System.out.println("[\tand sorting algorithms.\t\t\t]");
                for(int i = 0; i<49; i++){
                    System.out.print("="); 
                    Thread.sleep(100);
                }
                System.out.print('\u000c');
            } else{
                System.out.print("\nACCESS DENIED!");
                Thread.sleep(1500);
                System.out.print('\u000c');
            }
        } while(isCorrect == false);

    }

    public static void displayMain() throws InterruptedException{
        System.out.print('\u000c');
        System.out.println("\t<< MAIN MENU >>");
        System.out.println("[1] Searching Algorithms");
        System.out.println("[2] Sorting Algorithms");
        System.out.println("[3] Exit");
        System.out.print("Enter OPTION: ");
        char opt = input.next().charAt(0);
        switch (opt){
            case '1' :
            displaySearch();
            break;
            case '2' :
            displaySort();
            break;
            case '3' :
            System.out.print('\u000c');
            System.exit(0);
            default :
            System.out.println("Invalid input!");
            Thread.sleep(1000);
            displayMain();
        } 
    }

    public static void displaySearch() throws InterruptedException{
        System.out.print('\u000c');
        System.out.println("\t<< SEARCHING ALGORITHMS >>");
        System.out.println("[1] Linear Search");
        System.out.println("[2] Binary Search");
        System.out.println("[3] Go back to MAIN MENU");
        System.out.print("Enter OPTION: ");
        char opt = input.next().charAt(0);
        switch (opt){
            case '1' :
            linearSearch();
            break;
            case '2' :
            binarySearch();
            break;
            case '3' :
            displayMain();
            break;
            default :
            System.out.println("Invalid input!");
            Thread.sleep(1000);
            displaySearch();
        } 
    }

    public static void displaySort() throws InterruptedException{
        System.out.print('\u000c');
        System.out.println("\t<< SORTING ALGORITHMS >>");
        System.out.println("[1] Bubble Sort");
        System.out.println("[2] Selection Sort");
        System.out.println("[3] Insertion Sort");
        System.out.println("[4] Quick Sort");
        System.out.println("[5] Merge Sort");
        System.out.println("[6] Go back to MAIN MENU");
        System.out.print("Enter OPTION: ");
        char opt = input.next().charAt(0);
        switch (opt){
            case '1' :
            System.out.print('\u000c');
            System.out.println("\t<< BUBBLE SORT >>\n");
            int[] arr = inputElements();
            bubbleSort(arr);
            System.out.print("Sorted Elements: ");
            for(int i = 0; i<arr.length; i++){
                System.out.print(arr[i] + " ");           
            }
            break;
            case '2' :
            System.out.print('\u000c');
            System.out.println("\t<< SELECTION SORT >>\n");
            arr = inputElements();
            selectionSort(arr);
            System.out.print("Sorted Elements: ");
            for(int i = 0; i<arr.length; i++){
                System.out.print(arr[i] + " ");           
            }
            break;
            case '3' :
            System.out.print('\u000c');
            System.out.println("\t<< INSERTION SORT >>\n");
            arr = inputElements();
            insertionSort(arr);
            System.out.print("Sorted Elements: ");
            for(int i = 0; i<arr.length; i++){
                System.out.print(arr[i] + " ");           
            }
            break;
            case '4' :
            System.out.print('\u000c');
            System.out.println("\t<< QUICK SORT >>\n");
            arr = inputElements();
            quickSort(arr, 0, (arr.length - 1));
            System.out.print("\nSorted Elements: ");
            for(int i = 0; i<arr.length; i++){
                System.out.print(arr[i] + " ");           
            }
            break;
            case '5' :
            System.out.print('\u000c');
            System.out.println("\t<< MERGE SORT >>\n");
            arr = inputElements();
            System.out.print("\n");
            mergeSort(arr);
            System.out.print("Sorted Elements: ");
            for(int i = 0; i<=(arr.length-1); i++){
                System.out.print(arr[i] + " ");           
            }
            break;
            case '6' :
            displayMain();
            break;
            default :
            System.out.println("Invalid input!");
            Thread.sleep(1000);
            displaySort();
        } 
    }

    // ***********   SEARCHING FUNCTIONS   ************

    //LINEAR SEARCH
    public static void linearSearch() throws InterruptedException{
        System.out.print('\u000c');
        System.out.println("\t<< LINEAR SEARCH >>");
        int[] arr = inputElements();
        System.out.print("Enter number to search: ");
        int searchThis = input.nextInt();
        for(int i = 0; i<arr.length; i++){
            if(arr[i] == searchThis){
                System.out.println(searchThis + " is at index " + i + " of the array.");
                break;
            } else if(i == arr.length - 1)
                System.out.println("Element not found!");
        }
    }

    //BINARY SEARCH
    public static void binarySearch() throws InterruptedException{
        System.out.print('\u000c');
        System.out.println("\t<< BINARY SEARCH >>");
        int[] arr = inputElements();
        System.out.print("Enter number to search: ");
        int searchThis = input.nextInt();
        bubbleSortPassesHidden(arr);
        System.out.print("\nSorted elements: ");
        for(int i = 0; i<arr.length; i++){
            System.out.print(arr[i] + " ");           
        }
        int first  = 0;
        int last   = arr.length - 1;
        int middle = (first + last)/2;
        while( first <= last )
        {
            if ( arr[middle] < searchThis )
                first = middle + 1;    
            else if ( arr[middle] == searchThis ) 
            {
                System.out.print("\n" + searchThis + " is at index " + middle + " of the array.");
                break;
            }
            else
                last = middle - 1;
            middle = (first + last)/2;
        }
        if ( first > last )
            System.out.println("\nElement not found!");
    }
    // ***********   SORTING FUNCTIONS   ************

    //BUBBLE SORT
    public static void bubbleSort(int[] arr){
        System.out.print("\n");
        boolean swapped = true;
        int j = 0;
        int tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < arr.length - j; i++) {      
                if (arr[i] > arr[i + 1]) {             
                    tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    swapped = true;
                }
            }    
            if(swapped == true){
                System.out.print("Pass " + j + ": ");
                for(int x = 0; x < arr.length; x++){
                    System.out.print(arr[x] + " ");
                }
                System.out.print("\n");
            }
        }
    }

    //SELECTION SORT
    public static void selectionSort(int[] arr){
        System.out.print("\n");
        int i, j, minIndex, tmp;
        int n = arr.length;
        for (i = 0; i < n - 1; i++) {
            minIndex = i;
            for (j = i + 1; j < n; j++)
                if (arr[j] < arr[minIndex])
                    minIndex = j;
            if (minIndex != i) {
                tmp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = tmp;
            }
            System.out.print("Pass " + (i+1) + ": ");
            for(int x = 0; x < arr.length; x++){
                System.out.print(arr[x] + " ");
            }
            System.out.print("\n");
        }

    }

    //INSERTION SORT
    public static void insertionSort(int[] arr){
        System.out.print("\n");
        int i, j, newValue;
        for (i = 1; i < arr.length; i++) {
            //Pass Printing
            System.out.print("Pass " + (i) + ": ");
            for(int x = 0; x < arr.length; x++){
                System.out.print(arr[x] + " ");
            }
            System.out.print("\n");
            newValue = arr[i];
            j = i;
            while (j > 0 && arr[j - 1] > newValue) {
                arr[j] = arr[j - 1];
                j--;
            }
            arr[j] = newValue;

        }
    }

    //QUICK SORT
    public static void quickSort(int arr[], int start, int end){
        int i = start;                          
        int k = end;                            
        if (end - start >= 1)                   // check that there are at least two elements to sort
        {
            System.out.print("\n");
            System.out.print("[Pivot " + arr[start] + "] "); //this is for printing passes
            int pivot = arr[start];       // set the pivot as the first element in the partition
            while (k > i)                   // while the scan indices from left and right have not met,
            {
                while (arr[i] <= pivot && i <= end && k > i)  // from the left, look for elements greater than the pivot
                    i++;                                    
                while (arr[k] > pivot && k >= start && k >= i) // from the right look for elements smaller than the pivot
                    k--;                                        
                if (k > i)                                       // if the left scanner (i) is still smaller than the right scanner (k)
                    swap(arr, i, k);                                //swap the two values
            }
            swap(arr, start, k);          // aput the pivot in the middle by swapping it with the last element of the left partition
            for(int x = 0; x < arr.length; x++){    //this is for printing passes
                System.out.print(arr[x] + " ");
            }
            quickSort(arr, start, k - 1); // quicksort the left partition
            quickSort(arr, k + 1, end);   // quicksort the right partition
        }
        else    // if there is only one element in the partition, do not do any sorting
        {
            return;                     // the array is sorted, so exit
        }

    }

    public static void swap(int arr[], int index1, int index2) 
    {
        int temp = arr[index1];           // store the first value in a temp
        arr[index1] = arr[index2];      // copy the value of the second into the first
        arr[index2] = temp;               // copy the value of the temp into the second
    }

    //MERGE SORT
    public static int[] mergeSort(int arr[])
    {
        // if the array has more than 1 element, we need to split it and merge the sorted halves
        if(arr.length > 1)
        {
            int elementsInA1 = arr.length/2;    //determine what will be the size of the 2 sub-arrays
            int elementsInA2 = elementsInA1;
            if((arr.length % 2) == 1)           //if length is odd add 1 to the size of the second sub-array
                elementsInA2 += 1;
            int arr1[] = new int[elementsInA1];     //initialize sub-arrays
            int arr2[] = new int[elementsInA2];
            for(int i = 0; i < elementsInA1; i++)           //copy the elements into the sub-arrays
                arr1[i] = arr[i];
            for(int i = elementsInA1; i < elementsInA1 + elementsInA2; i++)
                arr2[i - elementsInA1] = arr[i];

            int random = (int )(Math.random() * 100000 + 1); //this random number will be the id of each level
            System.out.print("Split [" + random + "] ");
            for(int x = 0; x < arr1.length; x++){    //this is for printing passes
                System.out.print(arr1[x] + " ");
            }
            System.out.print("\t");
            for(int x = 0; x < arr2.length; x++){    //this is for printing passes
                System.out.print(arr2[x] + " ");
            }
            System.out.print("\n");
            //call mergesort again for each partition
            arr1 = mergeSort(arr1);
            arr2 = mergeSort(arr2);
            // the three variables below are indexes that we'll need for merging
            int i = 0, j = 0, k = 0;
            // the below loop will run until one of the sub-arrays becomes empty
            // in my implementation, it means until the index equals the length of the sub-array
            while(arr1.length != j && arr2.length != k)
            {
                if(arr1[j] < arr2[k]) //compare elements of arr1 and arr2 then copy the smaller one to the main array
                {
                    arr[i] = arr1[j]; 
                    i++; //increment the indices to move to the next slot to compare all elements and to fill the main array
                    j++;
                }
                else
                {
                    arr[i] = arr2[k];
                    i++;
                    k++;
                }
            }
            //there will be leftover elements after comparing, these are the largest elements
            //copy the remaining elements to the main array
            while(arr1.length != j)
            {
                arr[i] = arr1[j];
                i++;
                j++;
            }
            while(arr2.length != k)
            {
                arr[i] = arr2[k];
                i++;
                k++;
            }

            System.out.print("Merge [" + random + "] ");
            for(int x = 0; x < arr.length; x++){    //this is for printing passes
                System.out.print(arr[x] + " ");
            }
            System.out.print("\n");
        }
        // return the sorted array to the caller of the function
        return arr;
    }

    //*********   MISC METHODS   **********
    public static int[] inputElements() throws InterruptedException {
        int num_elements = 0;
        boolean isInputCorrect = false;

        do{
            System.out.print("Enter number of elements: ");
            num_elements = input.nextInt();
            if(num_elements < 2){
                System.out.print("\nEnter at least two elements! ");
                Thread.sleep(1500);
                System.out.print('\u000c');
                isInputCorrect = false;
            }
            else
                isInputCorrect = true;
        }while(isInputCorrect == false);
        int[] arr = new int[num_elements];
        for(int i = 0; i < num_elements; i++){
            System.out.print("Enter element " + (i+1) + ": ");
            arr[i] = input.nextInt();
        }
        return arr;
    }
    
    public static void bubbleSortPassesHidden(int[] arr){
        System.out.print("\n");
        boolean swapped = true;
        int j = 0;
        int tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < arr.length - j; i++) {      
                if (arr[i] > arr[i + 1]) {             
                    tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    swapped = true;
                }
            }    
        }
    }

}
